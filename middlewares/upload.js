const multer = require('multer')

const upload = multer({
    limits: {
        fileSize: 5000000
    },
    fileFilter(req, file, cb) {
        if(!file.originalname.match(/\.(png|jpeg|jpg|pdf)$/)){
            return cb(new Error('Please Upload a valid type of file'))
        }
        cb(undefined, true)
    }
})

module.exports = upload