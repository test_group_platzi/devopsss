const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')

const Experience = require('../models/experienceModel')

const testExperience = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    company: 'Test Company',
    website: 'www.testing.com',
    position: 'Tester officer',
    startDate: '01/01/2019',
    endDate: '12/12/2019',
    activities: []
}
const testExperience2 = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    company: 'Test Company2',
    website: 'www.testing2.com',
    position: 'Tester2 officer',
    startDate: '01/01/2019',
    endDate: '12/12/2019',
    activities: []
}

beforeEach(async() => {
    try {
        await Experience.deleteMany()
        await new Experience(testExperience2).save()
    } catch (error) {
        throw new Error(error)
    }
})

test('Should save a new Experience', async() => {
    const resp = await supertest(app)
        .post('/api/experience')
        .send(testExperience)
        .expect(201)
    
    const experience = await Experience.findById(resp.body._id)
    expect(experience).not.toBeNull()
    expect(resp.body).toEqual({...testExperience, __v:0})
})

test('Should delete a Experience', async() => {
    await supertest(app)
        .delete(`/api/experience/${testExperience2._id}`)
        .send()
        .expect(200)
    
    const experience = await Experience.findById(testExperience2._id)
    expect(experience).toBeNull()
})

test('Should edit a Experience', async() => {
    await supertest(app)
        .patch(`/api/experience/${testExperience2._id}`)
        .send({company: 'Edited Company SA'})
        .expect(200)
    
    const experience = await Experience.findById(testExperience2._id)
    expect(experience.company).not.toBe(testExperience2.company)
    expect(experience.company).toBe('Edited Company SA')

})

test('Should return the experiences', async() => {
    const resp = await supertest(app)
        .get('/api/experiences')
        .expect(200)
    expect(resp.body).toEqual([{...testExperience2, __v:0}])
})

test('Should return a empty array', async() => {
    await Experience.deleteMany()
    const resp = await supertest(app)
                .get('/api/experiences')
                .expect(200)
    expect(resp.body).toEqual([])
})