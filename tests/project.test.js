const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')

const Project = require('../models/projectModel')

const project1 = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    name: 'Test Project',
    description: 'This is the test Project1',
    link: 'www.test.com',
    skills: []
}
const project2 = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    name: 'Test Project2',
    description: 'This is the test Project2',
    link: 'www.test2.com',
    skills: []
}

beforeEach(async() => {
    try{
        await Project.deleteMany()
        await new Project(project2).save()
        
    } catch (error) {
        throw new Error(error)
    }
})

test('Should create a new project', async() => {
    await supertest(app)
        .post('/api/project')
        .field('data', JSON.stringify(project1))
        .attach('image', './tests/image/elCasoDeCristo.jpg')
        .expect(201)
    
    const project = await Project.findById(project1._id)
    expect(project).not.toBeNull()
})

test('Should delete a project', async() => {
    await supertest(app)
        .delete(`/api/project/${project2._id}`)
        .send()
        .expect(200)
    const project = await Project.findById(project2._id)
    expect(project).toBeNull()
})

test('Should return the projects', async() => {
    const resp = await supertest(app)
        .get('/api/projects')
        .expect(200)
    expect(resp.body).toEqual([{...project2, __v:0}])
})