const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')

const Education = require('../models/educationModel')

const education1 = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    institution: 'Testing University',
    degree: 'Testing Engineer',
    startDate: '01/01/2010',
    endDate: '01/01/2015',
    website: 'www.testing.edu'
}
const education2 = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    institution: 'Testing College',
    degree: 'Testing Technician',
    startDate: '01/01/2017',
    endDate: '01/01/2010',
    website: 'www.testingcollege.edu'
}

beforeEach(async() => {
    try {
        await Education.deleteMany()
        await new Education(education2).save()
    } catch (error) {
        throw new Error(error)
    }
})

test('Should save a new Education', async() => {
   await supertest(app)
        .post('/api/education')
        .send(education1)
        expect(201)

    const education = await Education.findById(education1._id)
    expect(education).not.toBeNull()
})

test('Should edit a Experience', async() => {
    await supertest(app)
        .patch(`/api/education/${education2._id}`)
        .send({institution: 'Test Institute'})
        .expect(200)

    const education = await Education.findById(education2._id)
    expect(education.institution).not.toBe(education2.institution)
    expect(education.institution).toBe('Test Institute')
})

test('Should get a Education', async() => {
    const resp = await supertest(app)
        .get('/api/educations')
        .expect(200)

    expect(resp.body).toEqual([{...education2, __v:0}])
})

test('should delete a Education', async() => {
    await supertest(app)
        .delete(`/api/education/${education2._id}`)
        .send()
        .expect(200)
    
        const education = await Education.findById(education2._id)
        expect(education).toBeNull()
})