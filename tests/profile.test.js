const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')

const Profile = require('../models/profileModel')

const profile1 = {
    _id: new mongoose.Types.ObjectId().toHexString(), 
    name: 'Tester Testero',
    label: 'Engineer Tester',
    location: 'MongoDB datafarm',
    email: 'test@tester.com',
    website: 'www.tester.com',
    twitter: 'tester1',
    github: 'testertestero1',
    instagram: 'tester1',
    linkedin: 'testertestero1',
    skills: [],
    about: []
}
const profile2 = {
    _id: new mongoose.Types.ObjectId().toHexString(), 
    name: 'Tester2',
    label: 'Engineer',
    location: 'My Home',
    email: 'test2@tester.com',
    website: 'www.tester2.com',
    twitter: 'tester2',
    github: 'testertestero2',
    instagram: 'tester2',
    linkedin: 'testertestero2',
    skills: [],
    about: []
}

beforeEach(async() => {
    try {
        await Profile.deleteMany()
        await new Profile(profile2).save()
    } catch (error) {
        throw new Error(error)
    }
})

test('Should create a new Profile', async() => {
    const resp = await supertest(app)
        .post('/api/new-profile')
        .send(profile1)
        .expect(201)

    const profile = await Profile.findById(profile1._id)
    expect(profile).not.toBeNull()
    expect(resp.body).toEqual({...profile1, __v:0})
})

test('Should edit the Profile', async() => {
    await supertest(app)
        .patch(`/api/profile/${profile2._id}`)
        .send({name: 'Elon Musk'})
        .expect(200)
    
    const profile = await Profile.findById(profile2._id)
    expect(profile.name).not.toBe(profile2.name)
    expect(profile.name).toBe('Elon Musk')
})

test('Should return profile', async() => {
    const resp = await supertest(app)
                    .get('/api/profile')
                    .expect(200)
    expect(resp.body).toEqual([{...profile2, __v:0}])
})

test('Should upload an image and return it', async() => {
    await supertest(app)
        .post(`/api/update-image/${profile2._id}`)
        .attach('image', './tests/image/elCasoDeCristo.jpg')
        .expect(200)

    const image = await supertest(app)
        .get(`/api/profile-image/${profile2._id}`)
        .expect(200)

    const profile = await Profile.findById(profile2._id)
    expect(profile.image).not.toBeNull()
    expect(image).not.toBeNull()
})