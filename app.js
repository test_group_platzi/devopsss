const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const profileRoutes = require('./routes/profileRoutes')
const experienceRoutes = require('./routes/experienceRoutes')
const educationRoutes = require('./routes/educationRoutes')
const projectRoutes = require('./routes/projectRoutes')

const app = express()

//Allow cross domain
let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PATCH,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

//DB Conection
const connectionString = process.env.DATABASE_URI
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology:true})

//Middlewares
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(allowCrossDomain)
app.use(express.static('client'))

//Routes
app.use('/api', profileRoutes)
app.use('/api', experienceRoutes)
app.use('/api', educationRoutes)
app.use('/api', projectRoutes)



module.exports = app