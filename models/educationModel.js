const mongoose = require('mongoose')

let educationSchema = new mongoose.Schema({
    institution: {
        type: String,
        trim: true
    },
    degree: {
        type: String,
        trim: true
    },
    startDate: {
        type: String,
        trim: true
    },
    endDate: {
        type: String,
        trim: true
    },
    website: {
        type: String,
        trim: true
    }
})

const Education = mongoose.model('Education', educationSchema)

module.exports = Education