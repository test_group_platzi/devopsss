const mongoose = require('mongoose')

let profileSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    label: {
        type: String,
        trim: true
    },
    image: {
        type: Buffer,
        select: false
    },
    location: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    website: {
        type: String,
        trim: true
    },
    twitter: {
        type: String,
        trim: true
    },
    github: {
        type: String,
        trim: true
    },
    instagram: {
        type: String,
        trim: true
    },
    linkedin: {
        type: String,
        trim: true
    },
    skills: [{
        type: String,
        trim: true
    }],
    about: [{
        type: String,
        trim: true
    }],
    curriculum: {
        type: Buffer,
        select: false
    }
})

const Profile = mongoose.model('Profile', profileSchema)

module.exports = Profile