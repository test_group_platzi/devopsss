const mongoose = require('mongoose')

let bookSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true
    },
    author: {
        type: String,
        trim: true
    },
    cover: {
        type: String,
        trim: true
    }
})

const Book = mongoose.model('Book', bookSchema)

module.exports = {Book}