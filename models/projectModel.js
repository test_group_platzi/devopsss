const mongoose = require('mongoose')

let projectSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    image: {
        type: Buffer,
        select: false
    },
    description: {
        type: String,
        trim: true
    },
    link: {
        type: String,
        trim: true
    },
    skills: [{
        type: String,
        trim: true
    }]
})

const Project = mongoose.model('Project', projectSchema)

module.exports = Project