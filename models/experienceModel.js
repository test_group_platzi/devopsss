const mongoose = require('mongoose')

let experienceSchema = new mongoose.Schema({
    company: {
        type: String,
        trim: true
    },
    logo: {
        type: String,
        trim: true
    },
    website: {
        type: String,
        trim: true
    },
    position: {
        type: String,
        trim: true
    },
    startDate: {
        type: String,
        trim: true
    },
    endDate: {
        type: String,
        trim: true
    },
    activities: [{
        type: String,
        trim: true
    }]
})

const Experience = mongoose.model('Experience', experienceSchema)

module.exports = Experience