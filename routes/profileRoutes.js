const express = require('express')
const router = express.Router()

const upload = require('../middlewares/upload')

const Profile = require('../models/profileModel')

router.post('/new-profile', async(req, res) => {
    const profile = new Profile(req.body)
    try {
        await profile.save()
        res.status(201).send(profile)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.patch('/profile/:id', async(req, res) => {
    try {
        const editedProfile = await Profile.findByIdAndUpdate(req.params.id, req.body).setOptions({useFindAndModify: false})
        res.status(200).send(editedProfile)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/update-image/:id', upload.single('image'), async(req, res) => {
    try {
        const profile = await Profile.findById(req.params.id)
        if (!profile) {
            return res.status(404).send()
        }
        profile.image = req.file.buffer
        await profile.save()
        res.status(200).send()
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/upload-curriculum/:id', upload.single('curriculum'), async(req, res) => {
    try {
        const profile = await Profile.findById(req.params.id)
        if(!profile) {
            return res.status(404).send()
        }
        profile.curriculum = req.file.buffer
        await profile.save()
        res.status(200).send()
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/curriculum/:id', async(req, res) => {
    try {
        const profile = await Profile.findById(req.params.id).select('+curriculum')
        if (!profile) {
            return res.status(404).send()
        }
        res.set('Content-Type', 'application/pdf')
        res.set('Content-Disposition', 'attachment; filename=Jehiel Martinez CV.pdf')
        res.status(200).send(profile.curriculum)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/profile', async(req, res) => {
    try {
        const profile = await Profile.find()
        if (!profile) {
            return res.status(404).send()
        }
        res.status(200).send(profile)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/profile-image/:id', async(req, res) => {
    try {
        const profile = await Profile.findById(req.params.id).select('+image')
        if (!profile) {
            return res.status(404).send()
        }
        res.set('Content-Type', 'image/jpg')
        res.status(200).send(profile.image)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router