const express = require('express')
const router = express.Router()

const Experience = require('../models/experienceModel')

router.post('/experience', async(req, res) => {
    
    const experience = new Experience(req.body)

    try {
        await experience.save()
        res.status(201).send(experience)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.delete('/experience/:id', async(req, res) => {
    try {
        await Experience.findByIdAndDelete(req.params.id)
        res.status(200).send()
    } catch (error) {
        res.status(404).send(error)
    }
})

router.patch('/experience/:id', async(req, res) => {
    try {
        const editedExperience = await Experience.findByIdAndUpdate(req.params.id, req.body).setOptions({useFindAndModify: false})
        res.status(200).send(editedExperience)
    } catch (error) {
        res.status(404).send(error)
    }
})

router.get('/experiences', async(req, res) => {
    try {
        const experiences = await Experience.find()
        res.status(200).send(experiences)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router