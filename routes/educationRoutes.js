const express = require('express')
const router = express.Router()

const Education = require('../models/educationModel')

router.post('/education', async(req, res) => {
    const education = new Education(req.body)

    try{
        await education.save()
        res.status(201).send(education)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.delete('/education/:id', async(req, res) => {
    try {
        await Education.findByIdAndDelete(req.params.id)
        res.status(200).send()
    } catch (error) {
        res.status(404).send(error)
    }
})

router.patch('/education/:id', async(req, res) => {
    try {
        const editedEducation = await Education.findByIdAndUpdate(req.params.id, req.body).setOptions({useFindAndModify: false})
        res.status(200).send(editedEducation)
    } catch (error) {
        res.status(404).send(error)
    }
})

router.get('/educations', async(req, res) => {
    try {
        const educations = await Education.find()
        res.status(200).send(educations)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router