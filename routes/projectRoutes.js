const express = require('express')
const router = express.Router()

const upload = require('../middlewares/upload')
const Project = require('../models/projectModel')

router.post('/project', upload.single('image'), async(req, res) => {
    const newProject = new Project(JSON.parse(req.body.data))
    newProject.image = req.file.buffer
    try {
        await newProject.save()
        res.status(201).send()
    } catch (error) {
        res.status(400).send(error)
    }
})

router.delete('/project/:id', async(req, res) => {
    try {
        const project = await Project.findByIdAndDelete(req.params.id)
        if(!project){
            return res.status(404).send()
        }
        res.status(200).send()
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/projects', async(req, res) => {
    try {
        const projects = await Project.find()
        res.status(200).send(projects)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/project-image/:id', async(req,res) => {
    try {
        const project = await Project.findById(req.params.id).select('+image')
        if(!project){
            return res.status(404).send()
        }
        res.set('Content-Type', 'image/jpg')
        res.status(200).send(project.image)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router
